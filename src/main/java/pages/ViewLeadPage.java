package pages;

import libraries.Annotations;

public class ViewLeadPage extends Annotations {
	
	public ViewLeadPage verifyFirstName() {
		String text = driver.findElementById("viewLead_firstName_sp")
		.getText();
		if(text.equals("Sethu")) {
			System.out.println("First name matches with input data");
		}else {
			System.err.println("First name not matches with input data");
		}
		return this;
	}
	
	public ViewLeadPage getID()
	{
		String name = driver.findElementByXPath("//span[@ id= 'viewLead_companyName_sp']").getText();
	    //System.out.println(name);
//Store the lead number in a variable	    
	   leadId =name.replaceAll("\\D","");
	    //System.out.println(name1);
	    return this;
	}
	public MyLeadsPage clickDeletebutton()
		{
		
	    driver.findElementByXPath("//a[@ class='subMenuButtonDangerous']").click();
	    return new MyLeadsPage();
	   	}


	
	
	
	
	
	
}
