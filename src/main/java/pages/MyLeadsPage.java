package pages;

import libraries.Annotations;

public class MyLeadsPage extends Annotations {
	
	public CreateLeadPage clickCreateLead() {
		driver.findElementByLinkText("Create Lead")
		.click();
		return new CreateLeadPage();
		
		}
	public FindLeadPage clickfindLead()
	{
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		return new FindLeadPage();
	}
}
