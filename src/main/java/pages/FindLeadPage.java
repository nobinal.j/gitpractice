package pages;

import libraries.Annotations;

public class FindLeadPage extends Annotations {
	
	public FindLeadPage typeLeadId()
	{
		driver.findElementByXPath("//input[@ name='id']").sendKeys(leadId);
		return this;
	}
	public FindLeadPage clickFindLeadbutton() throws InterruptedException
	{
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		return this;
		
	}
	 public FindLeadPage confirmDelete()
	 {
		 String norecords = driver.findElementByXPath("//div[@class = 'x-paging-info']").getText();
		   
		    if(norecords.equals("No records to display"))
		    {
		    	System.out.println("Record Deleted and not available for selection ");
		    }
		    else
		    {
		    	System.out.println("Test Case Failed");
		    }
		 return this;
	 }
}
